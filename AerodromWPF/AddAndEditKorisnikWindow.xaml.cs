﻿using AerodromWPF.Database;
using AerodromWPF.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using static AerodromWPF.Model.Korisnik;

namespace AerodromWPF
{
    public partial class AddAndEditKorisnikWindow : Window
    {
        public enum EOpcija { DODAVANJE, IZMENA }
        private EOpcija opcija;
        private Korisnik korisnik;
        public AddAndEditKorisnikWindow(Korisnik korisnik, EOpcija opcija = EOpcija.DODAVANJE)
        {
            InitializeComponent();
            this.korisnik = korisnik;
            this.opcija = opcija;

            this.DataContext = korisnik;

            foreach (var pol in Enum.GetValues(typeof(EPol)))
            {
                CBPol.Items.Add(pol);
            }

            foreach (var tip in Enum.GetValues(typeof(ETip)))
            {
                CBTipKorisnika.Items.Add(tip);
            }

            if(opcija.Equals(EOpcija.IZMENA))
            {
                TxtKorisnickoIme.IsEnabled = false;
            }

        }

        private bool KorisnikPostoji(string korisnickoime)
        {
            foreach (Korisnik k in Data.Instance.Korisnici)
            {
                if (k.KorisnickoIme.Equals(korisnickoime))
                {
                    return true;
                }
            }
            return false;
        }

        private bool ValidEmail()
        {
            if(TxtEmail.Text.Contains(".com") && TxtEmail.Text.Contains("@"))
            {
                return true;
            }
            return false;
        }

        private void Sacuvaj_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(TxtIme.Text) || string.IsNullOrEmpty(TxtPrezime.Text) || string.IsNullOrEmpty(TxtKorisnickoIme.Text) ||
                string.IsNullOrEmpty(TxtSifra.Text) || string.IsNullOrEmpty(TxtEmail.Text) || string.IsNullOrEmpty(TxtAdresa.Text) || string.IsNullOrEmpty(CBTipKorisnika.Text) ||
                string.IsNullOrEmpty(CBPol.Text))
            {
                MessageBox.Show("Sva polja moraju biti popunjena");
            }
            else if (opcija.Equals(EOpcija.IZMENA))
                    {
                if (ValidEmail())
                {
                    this.DialogResult = true;
                    Data.Instance.IzmeniKorisnika(korisnik);

                    this.Close();
                }
                else
                {
                    MessageBox.Show("Email nije u pravilnom formatu");
                }
                    }
                    else if (opcija.Equals(EOpcija.DODAVANJE))
                    {
                        if (!KorisnikPostoji(korisnik.KorisnickoIme) && ValidEmail())
                        {
                            this.DialogResult = true;
                            Data.Instance.Korisnici.Add(korisnik);
                            Data.Instance.DodajKorisnika(korisnik);

                            this.Close();
                        }
                        else
                        {
                            MessageBox.Show("Vec postoji korisnik sa tim korisnickim imenom ili email nije u pravilnom formatu");
                        }
                    }

                

        }

        private void Odustani_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}

