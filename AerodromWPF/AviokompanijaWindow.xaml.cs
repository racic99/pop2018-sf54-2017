﻿using AerodromWPF.Database;
using AerodromWPF.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AerodromWPF
{
    public partial class AviokompanijaWindow : Window
    {
        public AviokompanijaWindow()
        {
            InitializeComponent();
            DGAviokompanije.ItemsSource = Data.Instance.Aviokompanije;
            DGAviokompanije.IsSynchronizedWithCurrentItem = true;
        }

        private void BtnDodajAviokompaniju_Click(object sender, RoutedEventArgs e)
        {
            AddAndEditAviokompanija aea = new AddAndEditAviokompanija(new Aviokompanija());
            aea.ShowDialog();
        }

        private void BtnIzmeniAviokompaniju_Click(object sender, RoutedEventArgs e)
        {
            Aviokompanija selektAviokompanija = (Aviokompanija)DGAviokompanije.SelectedItem;
            if (selektAviokompanija != null)
            {
                Aviokompanija aAviokompanija = (Aviokompanija)selektAviokompanija.Clone();
                AddAndEditAviokompanija aea = new AddAndEditAviokompanija(selektAviokompanija, AddAndEditAviokompanija.EOpcija.IZMENA);
                if (aea.ShowDialog() != true)
                {
                    int index = IndeksAviokompanije(selektAviokompanija.Sifra);
                    Data.Instance.Aviokompanije[index] = aAviokompanija;
                }
            }
            else
            {
                MessageBox.Show("Nije selektovana aviokompanija! ");
            }
            DGAviokompanije.Items.Refresh();
        }

        private void BtnObrisiAviokompaniju_Click(object sender, RoutedEventArgs e)
        {
            Aviokompanija aviokompanija = (Aviokompanija)DGAviokompanije.SelectedItem;
            if (SelektovanaAviokompanija(aviokompanija) && aviokompanija.Aktivan)
            {
                if (MessageBox.Show("Da li ste sigurni?", "Potvrda", MessageBoxButton.YesNo).Equals(MessageBoxResult.Yes))
                {
                    int indeks = IndeksAviokompanije(aviokompanija.Sifra);
                    Data.Instance.Aviokompanije[indeks].Aktivan = false;
                    Data.Instance.ObrisiAviokompaniju(aviokompanija);
                }
            }

        }

        private int IndeksAviokompanije(String sifra)
        {
            int indeks = -1;
            for (int i = 0; i < Data.Instance.Aviokompanije.Count; i++)
            {
                if (Data.Instance.Aviokompanije[i].Sifra.Equals(sifra))
                {
                    indeks = i;
                    break;
                }
            }
            return indeks;
        }

        private bool SelektovanaAviokompanija(Aviokompanija aviokompanija)
        {
            if (aviokompanija == null)
            {
                MessageBox.Show("Nije selektovana aviokompanija! ");
                return false;
            }
            return true;
        }
    }
}
