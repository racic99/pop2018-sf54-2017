﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using AerodromWPF.Database;
using AerodromWPF.Model;

namespace AerodromWPF
{
    public partial class KupovinaKarteWindow : Window
    {

        private Karta karta;
        Korisnik korisnik;
        String CenaKarteLet;

        public KupovinaKarteWindow(Karta karta,Korisnik korisnik)
        {
            InitializeComponent();
            DGLetovi.ItemsSource = Data.Instance.Letovi;
            DGLetovi.IsSynchronizedWithCurrentItem = true;

            this.karta = karta;
            this.korisnik = korisnik;
            this.DataContext = karta;

            CBSifraLeta.ItemsSource = Data.Instance.Letovi.Select(l => l.BrojLeta);


            TxtPutnik.Text = korisnik.KorisnickoIme;

            CBKlasaKarte.Items.Add("Ekonomska klasa");
            CBKlasaKarte.Items.Add("Biznis klasa");

            CBKapija.Items.Add("1");
            CBKapija.Items.Add("2");
            CBKapija.Items.Add("3");
            CBKapija.Items.Add("4");
            CBKapija.Items.Add("5");


            CBBrojSedista.IsEnabled = false;
            TxtCenaKarte.IsEnabled = false;
            TxtPutnik.IsEnabled = false;
            PotvrdiKlasuButton.IsEnabled = false;
            PonistiKlasuButton.IsEnabled = false;
            CBKlasaKarte.IsEnabled = false;
            PotvrdiSifruButton.IsEnabled = false;
            PonistiSifruButton.IsEnabled = false;

            foreach (var letovi in Data.Instance.Letovi)
            {
                CenaKarteLet = letovi.CenaKarte.ToString();
            }

        }

        private bool SedistePostoji(string sediste, string sifra)
        {
            foreach (Karta k in Data.Instance.Karte)
            {
                if (k.BrojSedista.Equals(sediste) && k.SifraLeta.Equals(sifra))
                {
                    return true;
                }
            }
            return false;
        }

        private void PotvrdiSifruButton_Click(object sender, RoutedEventArgs e)
        {
            foreach (var letovi in Data.Instance.Letovi)
            {
                if (CBSifraLeta.SelectedItem.Equals(letovi.BrojLeta))
                {
                    TxtCenaKarte.Text = letovi.CenaKarte.ToString();
                    CBSifraLeta.IsEnabled = false;
                    CBKlasaKarte.IsEnabled = true;
                }
            }
        }

        private void PonistiSifruButton_Click(object sender, RoutedEventArgs e)
        {
            CBSifraLeta.IsEnabled = true;
            PotvrdiKlasuButton.IsEnabled = false;
            PonistiKlasuButton.IsEnabled = false;
            CBKlasaKarte.IsEnabled = false;
            CBSifraLeta.Text = "";
            TxtCenaKarte.Text = "";
            PotvrdiSifruButton.IsEnabled = false;
            PonistiSifruButton.IsEnabled = false;
            CBKlasaKarte.SelectedItem = null;
        }

        private void PotvrdiKlasuButton_Click(object sender, RoutedEventArgs e)
        {
            bool ekonomska = false;
            PotvrdiKlasuButton.IsEnabled = false;
            CBBrojSedista.IsEnabled = true;
            if (CBKlasaKarte.SelectedItem.Equals("Biznis klasa"))
            {
                ekonomska = false;
            }
            else
            {
                ekonomska = true;
            }
            if (ekonomska == false)
            {
                CenaKarteLet = TxtCenaKarte.Text;
                TxtCenaKarte.Text = (1.5 * float.Parse(TxtCenaKarte.Text)).ToString();
                CBBrojSedista.Items.Add("1");
                CBBrojSedista.Items.Add("2");
                CBBrojSedista.Items.Add("3");
                CBBrojSedista.Items.Add("4");
                CBBrojSedista.Items.Add("5");
                CBBrojSedista.Items.Add("6");
                CBBrojSedista.Items.Add("7");
                CBBrojSedista.Items.Add("8");
                CBBrojSedista.Items.Add("9");
                CBBrojSedista.Items.Add("10");

                CBKlasaKarte.IsEnabled = false;
            }
            else if (ekonomska == true)
            {
                CBBrojSedista.Items.Add("11");
                CBBrojSedista.Items.Add("12");
                CBBrojSedista.Items.Add("13");
                CBBrojSedista.Items.Add("14");
                CBBrojSedista.Items.Add("15");
                CBBrojSedista.Items.Add("16");
                CBBrojSedista.Items.Add("17");
                CBBrojSedista.Items.Add("18");
                CBBrojSedista.Items.Add("19");
                CBBrojSedista.Items.Add("20");
                CBBrojSedista.Items.Add("21");
                CBBrojSedista.Items.Add("22");
                CBBrojSedista.Items.Add("23");
                CBBrojSedista.Items.Add("24");
                CBBrojSedista.Items.Add("25");
                CBBrojSedista.Items.Add("26");
                CBBrojSedista.Items.Add("27");
                CBBrojSedista.Items.Add("28");
                CBBrojSedista.Items.Add("29");
                CBBrojSedista.Items.Add("30");
                CBKlasaKarte.IsEnabled = false;
            }

        }

        private void PonistiKlasuButton_Click(object sender, RoutedEventArgs e)
        {
            PotvrdiKlasuButton.IsEnabled = true;
            CBBrojSedista.Items.Clear();
            CBBrojSedista.Text = "";
            CBKlasaKarte.IsEnabled = true;
            CBKlasaKarte.SelectedItem = null;
            CBBrojSedista.IsEnabled = false;
            PotvrdiKlasuButton.IsEnabled = false;
            PonistiKlasuButton.IsEnabled = false;
            TxtCenaKarte.Text = CenaKarteLet;
        }

        private void KupiKartuButton_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(CBSifraLeta.Text) || string.IsNullOrEmpty(CBBrojSedista.Text) || string.IsNullOrEmpty(TxtPutnik.Text) ||
                string.IsNullOrEmpty(TxtCenaKarte.Text) || string.IsNullOrEmpty(CBKapija.Text)) 
            {
                MessageBox.Show("Sva polja moraju biti popunjena");
            }

            else if (!SedistePostoji(karta.BrojSedista,karta.SifraLeta))
            {
                karta.SifraLeta = CBSifraLeta.Text;
                karta.BrojSedista = CBBrojSedista.Text;
                karta.KorisnickoIme = TxtPutnik.Text;
                karta.KlasaKarte = CBKlasaKarte.Text;
                karta.CenaKarte = TxtCenaKarte.Text;
                karta.Kapija = CBKapija.Text;
                this.DialogResult = true;
                Data.Instance.Karte.Add(karta);
                Data.Instance.DodajKartu(karta);
                MessageBox.Show("Uspesno kupljena karta");
            }
            else
            {
                MessageBox.Show("Sediste koje ste izabrali je zauzeto");
            }

        }

        private void OdustaniButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void CBSifraLeta_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            PotvrdiSifruButton.IsEnabled = true;
            PonistiSifruButton.IsEnabled = true;
        }

        private void CBKlasaKarte_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (CBKlasaKarte.SelectedItem != null)
            {
                PotvrdiKlasuButton.IsEnabled = true;
                PonistiKlasuButton.IsEnabled = true;
            }
        }
    }
}
