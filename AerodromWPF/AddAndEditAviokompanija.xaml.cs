﻿using AerodromWPF.Database;
using AerodromWPF.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AerodromWPF
{
    public partial class AddAndEditAviokompanija : Window
    {
        public enum EOpcija { DODAVANJE, IZMENA }
        private EOpcija opcija;
        private Aviokompanija aviokompanija;
        public AddAndEditAviokompanija(Aviokompanija aviokompanija, EOpcija opcija = EOpcija.DODAVANJE)
        {
            InitializeComponent();
            this.aviokompanija = aviokompanija;
            this.opcija = opcija;

            this.DataContext = aviokompanija;
            TxtSifra.CharacterCasing = CharacterCasing.Upper;

            if (opcija.Equals(EOpcija.IZMENA))
            {
                TxtSifra.IsEnabled = false;

            }
        }

        private bool AviokompanijaPostoji(string sifra)
        {
            foreach (Aviokompanija a in Data.Instance.Aviokompanije)
            {
                if (a.Sifra.Equals(sifra))
                {
                    return true;
                }
            }
            return false;
        }

        private bool SifraDuzina()
        {
            if (TxtSifra.Text.Length > 3)
            {
                return true;
            }
            return false;
        }

        private void Sacuvaj_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(TxtSifra.Text) || string.IsNullOrEmpty(TxtNaziv.Text))
            {
                MessageBox.Show("Sva polja moraju biti popunjena");
            }
            else if (opcija.Equals(EOpcija.IZMENA))
            {

                this.DialogResult = true;
                Data.Instance.IzmeniAviokompanije(aviokompanija);
                this.Close();
            }

            else if (opcija.Equals(EOpcija.DODAVANJE))
            {
                if (!AviokompanijaPostoji(aviokompanija.Sifra) && !SifraDuzina())
                {
                    this.DialogResult = true;
                    Data.Instance.DodajAviokompaniju(aviokompanija);
                    Data.Instance.Aviokompanije.Add(aviokompanija);
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Vec postoji aviokompanija sa tom sifrom ili je sifra aviokompanije duza od 3 karaktera");
                }
            }

        }

        private void Odustani_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}
