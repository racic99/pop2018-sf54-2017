﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using AerodromWPF.Database;
using AerodromWPF.Model;

namespace AerodromWPF
{
    /// <summary>
    /// Interaction logic for LoginWindow.xaml
    /// </summary>
    public partial class LoginWindow : Window
    {


        public LoginWindow()
        {
            InitializeComponent();
        }

        private void RegButton_Click(object sender, RoutedEventArgs e)
        {
            Korisnik korisnik = new Korisnik();
            korisnik.Tip = ETip.PUTNIK;

            AddAndEditKorisnikWindow aek = new AddAndEditKorisnikWindow(korisnik, AddAndEditKorisnikWindow.EOpcija.DODAVANJE);
            aek.CBTipKorisnika.IsEnabled = false;
            aek.ShowDialog();
        }

        private void LoginButton_Click(object sender, RoutedEventArgs e)
        {

            if (string.IsNullOrEmpty(UsernameTxt.Text) || string.IsNullOrEmpty(SifraTxt.Password))
            {
                MessageBox.Show("Polja 'Korisnicko ime' i 'Sifra' moraju biti popunjeni ");
            }
            else
            {
                Korisnik korisnik = login(UsernameTxt.Text.Trim(), SifraTxt.Password.Trim());
                if (korisnik != null)
                {
                    UsernameTxt.Text = "";
                    SifraTxt.Password = "";
                    if (korisnik.Tip.Equals(ETip.ADMINISTRATOR))
                    {
                        AdminWindow aw = new AdminWindow(korisnik)
                        {
                            Title = "Glavni prozor - " + korisnik.KorisnickoIme
                        };
                        aw.ShowDialog();
                    }

                    else if (korisnik.Tip.Equals(ETip.PUTNIK))
                    {
                        PutnikWindow pu = new PutnikWindow(korisnik);
                        pu.ShowDialog();
                    }
                }
                else
                {
                    MessageBox.Show("Korisnicko ime ili sifra nisu ispravni");
                }

            }

        }

        private void IzlazButton_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Application.Current.Shutdown();
        }

        private void LetoviClick_Button(object sender, RoutedEventArgs e)
        {
            LetoviWindow let = new LetoviWindow();
            let.Height = 350;
            let.ShowDialog();
        }

        public Korisnik login(String username, String sifra)
        {
            foreach (var korisnik in Data.Instance.Korisnici)
            {
                if (korisnik.KorisnickoIme.Equals(username) && korisnik.Sifra.Equals(sifra))
                {
                    return korisnik;
                }
            }
            return null;
        }
    }
}
