﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AerodromWPF.Model
{
    public class Avion : INotifyPropertyChanged, ICloneable
    {
        public Avion()
        {
            aktivan = true;
        }

        private string brojLeta;

        public string BrojLeta
        {
            get { return brojLeta; }
            set { brojLeta = value; OnPropertyChanged("BrojLeta"); }
        }

        private string sedistaBiznis;

        public string SedistaBiznis
        {
            get { return sedistaBiznis; }
            set { sedistaBiznis = value; OnPropertyChanged("SedistaBiznis"); }
        }

        private string sedistaEkonomska;

        public string SedistaEkonomska
        {
            get { return sedistaEkonomska; }
            set { sedistaEkonomska = value; OnPropertyChanged("SedistaEkonomska"); }
        }

        private string nazivKompanije;

        public string NazivKompanije
        {
            get { return nazivKompanije; }
            set { nazivKompanije = value; OnPropertyChanged("NazivKompanije"); }
        }

        private bool aktivan;

        public bool Aktivan
        {
            get { return aktivan; }
            set { aktivan = value; OnPropertyChanged("Aktivan"); }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

        public override string ToString()
        {
            return $"Broj Leta {BrojLeta} Sedista biznis klase {SedistaBiznis} Sedista ekonomske klase {SedistaEkonomska} Naziv aviokompanije {NazivKompanije}";
        }

        public object Clone()
        {
            Avion noviAvion = new Avion
            {
                BrojLeta = this.BrojLeta,
                SedistaBiznis = this.SedistaBiznis,
                SedistaEkonomska = this.SedistaEkonomska,
                NazivKompanije = this.NazivKompanije,
                Aktivan = this.Aktivan
            };

            return noviAvion;
        }

    }
}
