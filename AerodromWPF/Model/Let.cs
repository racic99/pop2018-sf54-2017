﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AerodromWPF.Model
{
    public class Let : INotifyPropertyChanged, ICloneable
    {
        public Let()
        {
            aktivan = true;
        }

        private string pilot;

        public string Pilot
        {
            get { return pilot; }
            set { pilot = value; OnPropertyChanged("Pilot"); }
        }
    
        private string brojLeta;

        public string BrojLeta
        {
            get { return brojLeta; }
            set { brojLeta = value; OnPropertyChanged("BrojLeta"); }
        }

        private string destinacija;

        public string Destinacija
        {
            get { return destinacija; }
            set { destinacija = value; OnPropertyChanged("Destinacija"); }
        }

        private string odrediste;

        public string Odrediste
        {
            get { return odrediste; }
            set { odrediste = value; OnPropertyChanged("Odrediste"); }
        }

        private string cenaKarte;

        public string CenaKarte
        {
            get { return cenaKarte; }
            set { cenaKarte = value; OnPropertyChanged("CenaKarte"); }
        }


        private string vremePolaska;

        public string VremePolaska
        {
            get { return vremePolaska; }
            set { vremePolaska = value; OnPropertyChanged("VremePolaska"); }
        }

        private string vremeDolaska;

        public string VremeDolaska
        {
            get { return vremeDolaska; }
            set { vremeDolaska = value; OnPropertyChanged("VremeDolaska"); }
        }

        private bool aktivan;
        public bool Aktivan
        {
            get { return aktivan; }
            set { aktivan = value; OnPropertyChanged("Aktivan"); }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

        public override string ToString()
        {
            return $" Pilot {Pilot} Sifta {BrojLeta} Destinacija {Destinacija} Odrediste {Odrediste} Cena karte {CenaKarte} Vreme polaska {VremePolaska} Vreme dolaska {VremeDolaska}";
        }

        public object Clone()
        {
            Let noviLet = new Let
            {
                Pilot = this.Pilot,
                BrojLeta = this.BrojLeta,
                Destinacija = this.Destinacija,
                Odrediste = this.Odrediste,
                CenaKarte = this.CenaKarte,
                VremePolaska = this.VremePolaska,
                VremeDolaska = this.VremeDolaska,
                Aktivan = this.Aktivan
            };

            return noviLet;
        }
    }
}
