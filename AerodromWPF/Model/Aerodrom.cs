﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AerodromWPF.Model
{
    public class Aerodrom : INotifyPropertyChanged, ICloneable
    {
        public Aerodrom()
        {
            aktivan = true;
        }
            

        private string sifra;

        public string Sifra
        {
            get { return sifra; }
            set { sifra = value; OnPropertyChanged("Sifra"); }
        }

        private string ime;

        public string Ime
        {
            get { return ime; }
            set { ime = value; OnPropertyChanged("Ime"); }
        }

        private string grad;

        public string Grad
        {
            get { return grad; }
            set { grad = value; OnPropertyChanged("Grad"); }
        }

        private bool aktivan;

        public bool Aktivan
        {
            get { return aktivan; }
            set { aktivan = value; OnPropertyChanged("Aktivan"); }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

        public override string ToString()
        {
            return $"Sifra {Sifra} Ime {Ime} Grad {Grad}";
        }

        public object Clone()
        {
            Aerodrom noviAerodrom = new Aerodrom
            {
                Sifra = this.Sifra,
                Ime = this.Ime,
                Grad = this.Grad,
                Aktivan = this.Aktivan
            };

            return noviAerodrom;
        }

    }
}
