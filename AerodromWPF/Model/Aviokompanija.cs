﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AerodromWPF.Model
{
    public class Aviokompanija : INotifyPropertyChanged, ICloneable
    {
        public Aviokompanija()
        {
            aktivan = true;
        }

        private string sifra;

        public string Sifra
        {
            get { return sifra; }
            set { sifra = value; OnPropertyChanged("Sifra"); }
        }

        private string naziv;

        public string Naziv
        {
            get { return naziv; }
            set { naziv = value; OnPropertyChanged("Letovi"); }
        }


        private bool aktivan;

        public bool Aktivan
        {
            get { return aktivan; }
            set { aktivan = value; OnPropertyChanged("Aktivan"); }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

        public override string ToString()
        {
            return $"Red {Sifra} Sediste {Naziv}";
        }

        public object Clone()
        {
            Aviokompanija novaAviokompanija = new Aviokompanija
            {
                Sifra = this.Sifra,
                Naziv = this.Naziv,
                Aktivan = this.Aktivan
            };

            return novaAviokompanija;
        }

    }
}
