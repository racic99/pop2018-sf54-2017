﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AerodromWPF.Model
{
    public class Karta : INotifyPropertyChanged, ICloneable
    {

        private string sifraLeta;
        public string SifraLeta
        {
            get { return sifraLeta; }
            set { sifraLeta = value; OnPropertyChanged("SifraLeta"); }
        }


        private string brojSedista;
        public string BrojSedista
        {
            get { return brojSedista; }
            set { brojSedista = value; OnPropertyChanged("BrojSedista"); }
        }


        private string korisnickoIme;
        public string KorisnickoIme
        {
            get { return korisnickoIme; }
            set { korisnickoIme = value; OnPropertyChanged("Putnik"); }
        }


        private string klasaKarte;
        public string KlasaKarte
        {
            get { return klasaKarte; }
            set { klasaKarte = value; OnPropertyChanged("KlasaKarte"); }
        }



        private string cenaKarte;
        public string CenaKarte
        {
            get { return cenaKarte; }
            set { cenaKarte = value; OnPropertyChanged("CenaKarte"); }
        }

        private string kapija;

        public string Kapija
        {
            get { return kapija; }
            set { kapija = value; OnPropertyChanged("Kapija"); }
        }

        private bool aktivan;
        public bool Aktivan
        {
            get { return aktivan; }
            set { aktivan = value; OnPropertyChanged("Aktivan"); }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }

        public object Clone()
        {
            Karta nova = new Karta
            {
                SifraLeta = this.SifraLeta,
                BrojSedista = this.BrojSedista,
                KorisnickoIme = this.KorisnickoIme,
                KlasaKarte = this.KlasaKarte,
                CenaKarte = this.CenaKarte,
                Kapija = this.Kapija,
                Aktivan = this.Aktivan
            };
            return nova;
        }



    }
}
