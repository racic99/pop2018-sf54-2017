﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AerodromWPF.Model
{
    public class Sediste : INotifyPropertyChanged, ICloneable
    {
        private string red;

        public string Red
        {
            get { return red; }
            set { red = value; OnPropertyChanged("Red"); }
        }

        private string sedisteURedu;

        public string SedisteURedu
        {
            get { return sedisteURedu; }
            set { sedisteURedu = value; OnPropertyChanged("SedisteURedu"); }
        }


        private bool aktivan;

        public bool Aktivan
        {
            get { return aktivan; }
            set { aktivan = value; OnPropertyChanged("Aktivan"); }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

        public override string ToString()
        {
            return $"Red {Red} Sediste {SedisteURedu}";
        }

        public object Clone()
        {
            Sediste novoSediste = new Sediste
            {
                Red = this.Red,
                SedisteURedu = this.SedisteURedu,
                Aktivan = this.Aktivan
            };

            return novoSediste;
        }

    }
}
