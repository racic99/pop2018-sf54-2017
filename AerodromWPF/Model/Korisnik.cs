﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AerodromWPF.Model
{
    public class Korisnik : INotifyPropertyChanged, ICloneable
    {
        public Korisnik()
        {
            aktivan = true;
        }

        private EPol pol;
        public EPol Pol
        {
            get { return pol; }
            set { pol = value; }
        }

        private ETip tip;
        public ETip Tip
        {
            get { return tip; }
            set { tip = value; }
        }



        public event PropertyChangedEventHandler PropertyChanged;

        private string ime;

        public string Ime
        {
            get { return ime; }
            set { ime = value; OnPropertyChanged("Ime"); }
        }

        private string prezime;

        public string Prezime
        {
            get { return prezime; }
            set { prezime = value; OnPropertyChanged("Prezime"); }
        }

        private string email;

        public string Email
        {
            get { return email; }
            set { email = value; OnPropertyChanged("Email"); }
        }

        private string adresa;

        public string Adresa
        {
            get { return adresa; }
            set { adresa = value; OnPropertyChanged("Adresa"); }
        }

        private string korisnickoIme;

        public string KorisnickoIme
        {
            get { return korisnickoIme; }
            set { korisnickoIme = value; OnPropertyChanged("KorisnickoIme"); }
        }

        private string sifra;

        public string Sifra
        {
            get { return sifra; }
            set { sifra = value; OnPropertyChanged("Sifra"); }
        }

        private bool aktivan;
        public bool Aktivan
        {
            get { return aktivan; }
            set { aktivan = value; OnPropertyChanged("Aktivan"); }
        }

        public void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

        public override string ToString()
        {
            return $"Name {Ime} Lastname {Prezime} Email {Email} Adress {Adresa}";
        }
        public object Clone()
        {
            Korisnik noviKorisnik = new Korisnik
            {
                Ime = this.Ime,
                Prezime = this.Prezime,
                Email = this.Email,
                Adresa = this.Adresa,
                Pol = this.Pol,
                KorisnickoIme = this.KorisnickoIme,
                Sifra = this.Sifra,
                Tip = this.Tip,
                Aktivan = this.Aktivan
            };

            return noviKorisnik;
        }
    }
}
