﻿using AerodromWPF.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace AerodromWPF.Database
{
    class Data
    {
        public const string CONNECTION_STRING = @"Data Source=CREATOR\SQLEXPRESS;Initial Catalog=POPBaza;Integrated Security=True;";

        public ObservableCollection<Korisnik> Korisnici { get; set; }
        public ObservableCollection<Aerodrom> Aerodromi { get; set; }
        public ObservableCollection<Let> Letovi { get; set; }
        public ObservableCollection<Avion> Avioni { get; set; }
        public ObservableCollection<Aviokompanija> Aviokompanije { get; set; }
        public ObservableCollection<Karta> Karte { get; set; }
        public String UlogovanKorisnik { get; set; }

        private Data()
        {
            Korisnici = new ObservableCollection<Korisnik>();
            Aerodromi = new ObservableCollection<Aerodrom>();
            Letovi = new ObservableCollection<Let>();
            Avioni = new ObservableCollection<Avion>();
            Aviokompanije = new ObservableCollection<Aviokompanija>();
            Karte = new ObservableCollection<Karta>();

            UcitajSveAerodrome();
            UcitajSveAvione();
            UcitajSveKorisnike();
            UcitajSveLetove();
            UcitajSveAviokompanije();
            UcitajSveKarte();
        }

        private static Data _instance = null;

        public static Data Instance
        {

            get
            {
                if (_instance == null)
                    _instance = new Data();
                return _instance;
            }
        }

        public void UcitajSveAerodrome()
        {
            using (SqlConnection connection = new SqlConnection(CONNECTION_STRING))
            {
                    connection.Open();
                    DataSet ds = new DataSet();

                    SqlCommand aerodromiCommand = connection.CreateCommand();
                    aerodromiCommand.CommandText = @"Select distinct * from Aerodrom where aktivan=1";
                    SqlDataAdapter daAerodromi = new SqlDataAdapter();
                    daAerodromi.SelectCommand = aerodromiCommand;
                    daAerodromi.Fill(ds, "Aerodrom");

                    foreach (DataRow row in ds.Tables["Aerodrom"].Rows)
                    {
                        Aerodrom a = new Aerodrom();
                        a.Sifra = (string)row["sifra"];
                        a.Ime = (string)row["ime"];
                        a.Grad = (string)row["grad"];
                        a.Aktivan = (bool)row["aktivan"];

                        Aerodromi.Add(a);
                    }

                    connection.Close();
                }
                
            }
        
        public void DodajAerodrom(Aerodrom a)
        {
            using (SqlConnection connection = new SqlConnection(CONNECTION_STRING))
            {
                connection.Open();

                SqlCommand command = connection.CreateCommand();
                command.CommandText = @"INSERT INTO Aerodrom values(@sifra,@ime,@grad,1)";

                command.Parameters.Add(new SqlParameter("@sifra", a.Sifra));
                command.Parameters.Add(new SqlParameter("@ime", a.Ime));
                command.Parameters.Add(new SqlParameter("@grad", a.Grad));
                command.Parameters.Add(new SqlParameter("@aktivan", a.Aktivan));

                command.ExecuteNonQuery();

                connection.Close();

            }

        }

        public void ObrisiAerodrom(Aerodrom a)
        {
            using (SqlConnection connection = new SqlConnection(CONNECTION_STRING))
            {

                connection.Open();

                SqlCommand command = connection.CreateCommand();
                command.CommandText = @"update Aerodrom set aktivan='0' where sifra=@sifra";

                command.Parameters.Add(new SqlParameter("@sifra", a.Sifra));

                command.ExecuteNonQuery();

                connection.Close();

            }
        }

        public void IzmeniAerodrom(Aerodrom a)
        {
            using (SqlConnection connection = new SqlConnection(CONNECTION_STRING))
            {
                connection.Open();

                SqlCommand command = connection.CreateCommand();
                command.CommandText = @"UPDATE Aerodrom SET grad=@grad, ime=@ime  WHERE sifra=@sifra";

                command.Parameters.Add(new SqlParameter("@ime", a.Ime));
                command.Parameters.Add(new SqlParameter("@grad", a.Grad));
                command.Parameters.Add(new SqlParameter("@sifra", a.Sifra));

                command.ExecuteNonQuery();

                connection.Close();
            }
        }

        public void UcitajSveLetove()
        {
            using (SqlConnection connection = new SqlConnection(CONNECTION_STRING))
            {
                connection.Open();
                DataSet ds = new DataSet();

                SqlCommand letCommand = connection.CreateCommand();
                letCommand.CommandText = @"Select distinct * from Let where aktivan=1";
                SqlDataAdapter dalet = new SqlDataAdapter();
                dalet.SelectCommand = letCommand;
                dalet.Fill(ds, "Let");

                foreach (DataRow row in ds.Tables["Let"].Rows)
                {
                    Let l = new Let();
                    l.Pilot = (string)row["pilot"];
                    l.BrojLeta = (string)row["brojleta"];
                    l.Destinacija = (string)row["destinacija"];
                    l.Odrediste = (string)row["odrediste"];
                    l.CenaKarte = (string)row["cenakarte"];
                    l.VremePolaska = (string)row["vremepolaska"];
                    l.VremeDolaska = (string)row["vremedolaska"];
                    l.Aktivan = (bool)row["aktivan"];

                    Letovi.Add(l);
                }
                connection.Close();
            }
        }

        public void DodajLet(Let l)
        {
            using (SqlConnection connection = new SqlConnection(CONNECTION_STRING))
            {
                connection.Open();

                SqlCommand command = connection.CreateCommand();
                command.CommandText = @"INSERT INTO Let values(@pilot,@brojleta,@destinacija,@odrediste,@cenakarte,@vremepolaska,@vremedolaska,1)";

                command.Parameters.Add(new SqlParameter("@pilot", l.Pilot));
                command.Parameters.Add(new SqlParameter("@brojleta", l.BrojLeta));
                command.Parameters.Add(new SqlParameter("@destinacija", l.Destinacija));
                command.Parameters.Add(new SqlParameter("@odrediste", l.Odrediste));
                command.Parameters.Add(new SqlParameter("@cenakarte", l.CenaKarte));
                command.Parameters.Add(new SqlParameter("@vremepolaska", l.VremePolaska));
                command.Parameters.Add(new SqlParameter("@vremedolaska", l.VremeDolaska));

                command.ExecuteNonQuery();

                connection.Close();
            }

        }

        public void ObrisiLet(Let l)
        {
            using (SqlConnection connection = new SqlConnection(CONNECTION_STRING))
            {

                connection.Open();

                SqlCommand command = connection.CreateCommand();
                command.CommandText = @"update Let set aktivan=0 where brojleta=@brojleta";

                command.Parameters.Add(new SqlParameter("@brojleta", l.BrojLeta));

                command.ExecuteNonQuery();

                connection.Close();

            }
        }

        public void IzmeniLet(Let l)
        {
            using (SqlConnection connection = new SqlConnection(CONNECTION_STRING))
            {
                connection.Open();

                SqlCommand command = connection.CreateCommand();
                command.CommandText = @"UPDATE Let SET pilot=@pilot,brojleta=@brojleta,destinacija=@destinacija,odrediste=@odrediste,
                                        cenakarte=@cenakarte,vremepolaska=@vremepolaska,vremedolaska=@vremedolaska where brojleta=@brojleta";

                command.Parameters.Add(new SqlParameter("@pilot", l.Pilot));
                command.Parameters.Add(new SqlParameter("@brojleta", l.BrojLeta));
                command.Parameters.Add(new SqlParameter("@destinacija", l.Destinacija));
                command.Parameters.Add(new SqlParameter("@odrediste", l.Odrediste));
                command.Parameters.Add(new SqlParameter("@cenakarte", l.CenaKarte));
                command.Parameters.Add(new SqlParameter("@vremepolaska", l.VremePolaska));
                command.Parameters.Add(new SqlParameter("@vremedolaska", l.VremeDolaska));


                command.ExecuteNonQuery();
                connection.Close();
            }
        }

        public void UcitajSveKorisnike()
        {
            using (SqlConnection connection = new SqlConnection(CONNECTION_STRING))
            {
                connection.Open();
                DataSet ds = new DataSet();

                SqlCommand korisnikCommand = connection.CreateCommand();
                korisnikCommand.CommandText = @"Select distinct * from Korisnik where aktivan=1";
                SqlDataAdapter daKorisnici = new SqlDataAdapter();
                daKorisnici.SelectCommand = korisnikCommand;
                daKorisnici.Fill(ds, "Korisnik");

                foreach (DataRow row in ds.Tables["Korisnik"].Rows)
                {
                    Korisnik k = new Korisnik();
                    k.Ime = (string)row["ime"];
                    k.Prezime = (string)row["prezime"];
                    k.KorisnickoIme = (string)row["korime"];
                    k.Sifra = (string)row["sifra"];
                    k.Email = (string)row["email"];
                    k.Adresa = (string)row["adresa"];
                    k.Tip = (ETip)row["tip"]; 
                    k.Pol = (EPol)row["pol"];
                    k.Aktivan = (bool)row["aktivan"];

                    Korisnici.Add(k);
                }
                connection.Close();
            }
        }

        public void DodajKorisnika(Korisnik k)
        {
            using (SqlConnection connection = new SqlConnection(CONNECTION_STRING))
            {
                connection.Open();

                SqlCommand command = connection.CreateCommand();
                command.CommandText = @"INSERT INTO Korisnik values(@ime,@prezime,@korime,@sifra,@email,@adresa,@tip,@pol,1)";

                command.Parameters.Add(new SqlParameter("@ime", k.Ime));
                command.Parameters.Add(new SqlParameter("@prezime", k.Prezime));
                command.Parameters.Add(new SqlParameter("@korime", k.KorisnickoIme));
                command.Parameters.Add(new SqlParameter("@sifra", k.Sifra));
                command.Parameters.Add(new SqlParameter("@email", k.Email));
                command.Parameters.Add(new SqlParameter("@adresa", k.Adresa));
                command.Parameters.Add(new SqlParameter("@tip", k.Tip));
                command.Parameters.Add(new SqlParameter("@pol", k.Pol));
                command.Parameters.Add(new SqlParameter("@aktivan", k.Aktivan));

                command.ExecuteNonQuery();

                connection.Close();
            }

        }

        public void ObrisiKorisnika(Korisnik k)
        {
            using (SqlConnection connection = new SqlConnection(CONNECTION_STRING))
            {

                connection.Open();

                SqlCommand command = connection.CreateCommand();
                command.CommandText = @"update Korisnik set aktivan=0 where korime=@korime";

                command.Parameters.Add(new SqlParameter("@korime", k.KorisnickoIme));

                command.ExecuteNonQuery();
                connection.Close();
            }
        }

        public void IzmeniKorisnika(Korisnik k)
        {
            using (SqlConnection connection = new SqlConnection(CONNECTION_STRING))
            {
                connection.Open();

                SqlCommand command = connection.CreateCommand();
                command.CommandText = @"UPDATE Korisnik SET ime=@ime, prezime=@prezime,sifra=@sifra,email=@email,adresa=@adresa,tip=@tip,pol=@pol WHERE korime=@korime";

                command.Parameters.Add(new SqlParameter("@ime", k.Ime));
                command.Parameters.Add(new SqlParameter("@prezime", k.Prezime));
                command.Parameters.Add(new SqlParameter("@korime", k.KorisnickoIme));
                command.Parameters.Add(new SqlParameter("@sifra", k.Sifra));
                command.Parameters.Add(new SqlParameter("@email", k.Email));
                command.Parameters.Add(new SqlParameter("@adresa", k.Adresa));
                command.Parameters.Add(new SqlParameter("@tip", k.Tip));
                command.Parameters.Add(new SqlParameter("@pol", k.Pol));
                command.Parameters.Add(new SqlParameter("@aktivan", k.Aktivan));

                command.ExecuteNonQuery();
                connection.Close();
            }
        }

        public void UcitajSveAvione()
        {
            using (SqlConnection connection = new SqlConnection(CONNECTION_STRING))
            {
                connection.Open();
                DataSet ds = new DataSet();

                SqlCommand avionCommand = connection.CreateCommand();
                avionCommand.CommandText = @"Select distinct * from Avion where aktivan=1";
                SqlDataAdapter daAvioni = new SqlDataAdapter();
                daAvioni.SelectCommand = avionCommand;
                daAvioni.Fill(ds, "Avion");

                foreach (DataRow row in ds.Tables["Avion"].Rows)
                {

                    Avion a = new Avion();

                    a.BrojLeta = (string)row["brojleta"];
                    a.SedistaBiznis = (string)row["sedistabiz"];
                    a.SedistaEkonomska = (string)row["sedistaeko"];
                    a.NazivKompanije = (string)row["nazivkompanije"];
                    a.Aktivan = (bool)row["aktivan"];


                    Avioni.Add(a);
                }
                connection.Close();
            }
        }
        public void DodajAvion(Avion a)
        {
            using (SqlConnection connection = new SqlConnection(CONNECTION_STRING))
            {
                connection.Open();

                SqlCommand command = connection.CreateCommand();
                command.CommandText = @"INSERT INTO Avion values(@brojleta,@sedistabiz,@sedistaeko,@nazivkompanije,1)";

                command.Parameters.Add(new SqlParameter("@brojleta", a.BrojLeta));
                command.Parameters.Add(new SqlParameter("@sedistabiz", a.SedistaBiznis));
                command.Parameters.Add(new SqlParameter("@sedistaeko", a.SedistaEkonomska));
                command.Parameters.Add(new SqlParameter("@nazivkompanije", a.NazivKompanije));
                command.Parameters.Add(new SqlParameter("@aktivan", a.Aktivan));

                command.ExecuteNonQuery();

                connection.Close();
            }

        }

        public void ObrisiAvion(Avion a)
        {
            using (SqlConnection connection = new SqlConnection(CONNECTION_STRING))
            {

                connection.Open();

                SqlCommand command = connection.CreateCommand();
                command.CommandText = @"update Avion set aktivan=0 where brojleta=@brojleta";

                command.Parameters.Add(new SqlParameter("@brojleta", a.BrojLeta));

                command.ExecuteNonQuery();
                connection.Close();
            }
        }

        public void IzmeniAvion(Avion a)
        {
            using (SqlConnection connection = new SqlConnection(CONNECTION_STRING))
            {
                connection.Open();

                SqlCommand command = connection.CreateCommand();
                command.CommandText = @"UPDATE Avion SET sedistabiz=@sedistabiz,sedistaeko=@sedistaeko,nazivkompanije=@nazivkompanije WHERE brojleta=@brojleta";

                command.Parameters.Add(new SqlParameter("@brojleta", a.BrojLeta));
                command.Parameters.Add(new SqlParameter("@sedistabiz", a.SedistaBiznis));
                command.Parameters.Add(new SqlParameter("@sedistaeko", a.SedistaEkonomska));
                command.Parameters.Add(new SqlParameter("@nazivkompanije", a.NazivKompanije));
                command.Parameters.Add(new SqlParameter("@aktivan", a.Aktivan));

                command.ExecuteNonQuery();
                connection.Close();
            }
        }

        public void UcitajSveAviokompanije()
        {
            using (SqlConnection connection = new SqlConnection(CONNECTION_STRING))
            {
                connection.Open();
                DataSet ds = new DataSet();

                SqlCommand aviokompanijeCommand = connection.CreateCommand();
                aviokompanijeCommand.CommandText = @"Select distinct * from Aviokompanija where aktivan=1";
                SqlDataAdapter daAviokompanije = new SqlDataAdapter();
                daAviokompanije.SelectCommand = aviokompanijeCommand;
                daAviokompanije.Fill(ds, "Aviokompanija");

                foreach (DataRow row in ds.Tables["Aviokompanija"].Rows)
                {
                    Aviokompanija a = new Aviokompanija();
                    a.Sifra = (string)row["sifra"];
                    a.Naziv = (string)row["naziv"];
                    a.Aktivan = (bool)row["aktivan"];

                    Aviokompanije.Add(a);
                }
                connection.Close();
            }
        }

        public void DodajAviokompaniju(Aviokompanija a)
        {
            using (SqlConnection connection = new SqlConnection(CONNECTION_STRING))
            {
                connection.Open();

                SqlCommand command = connection.CreateCommand();
                command.CommandText = @"INSERT INTO Aviokompanija values(@sifra,@naziv,1)";

                command.Parameters.Add(new SqlParameter("@sifra", a.Sifra));
                command.Parameters.Add(new SqlParameter("@naziv", a.Naziv));

                command.ExecuteNonQuery();

                connection.Close();
            }

        }

        public void ObrisiAviokompaniju(Aviokompanija a)
        {
            using (SqlConnection connection = new SqlConnection(CONNECTION_STRING))
            {

                connection.Open();

                SqlCommand command = connection.CreateCommand();
                command.CommandText = @"update Aviokompanija set aktivan=0 where sifra=@sifra";

                command.Parameters.Add(new SqlParameter("@sifra", a.Sifra));

                command.ExecuteNonQuery();
                connection.Close();
            }
        }

        public void IzmeniAviokompanije(Aviokompanija a)
        {
            using (SqlConnection connection = new SqlConnection(CONNECTION_STRING))
            {
                connection.Open();

                SqlCommand command = connection.CreateCommand();
                command.CommandText = @"UPDATE Aviokompanija SET naziv=@naziv WHERE sifra=@sifra";

                command.Parameters.Add(new SqlParameter("@naziv", a.Naziv));
                command.Parameters.Add(new SqlParameter("@sifra", a.Sifra));

                command.ExecuteNonQuery();
                connection.Close();
            }
        }

        public void UcitajSveKarte()
        {
            using (SqlConnection connection = new SqlConnection(CONNECTION_STRING))
            {
                connection.Open();
                DataSet ds = new DataSet();

                SqlCommand karteCommand = connection.CreateCommand();
                karteCommand.CommandText = @"Select distinct * from Karta where aktivan=1";
                SqlDataAdapter daKarte = new SqlDataAdapter();
                daKarte.SelectCommand = karteCommand;
                daKarte.Fill(ds, "Karta");

                foreach (DataRow row in ds.Tables["Karta"].Rows)
                {
                    Karta k = new Karta();
                    k.SifraLeta = (string)row["sifraleta"];
                    k.BrojSedista = (string)row["brojsedista"];
                    k.KorisnickoIme = (string)row["putnik"];
                    k.KlasaKarte = (string)row["klasakarte"];
                    k.CenaKarte = (string)row["cenakarte"];
                    k.Kapija = (string)row["kapija"];
                    k.Aktivan = (bool)row["aktivan"];

                    Karte.Add(k);
                }

                connection.Close();
            }

        }

        public void DodajKartu(Karta k)
        {
            using (SqlConnection connection = new SqlConnection(CONNECTION_STRING))
            {
                connection.Open();

                SqlCommand command = connection.CreateCommand();
                command.CommandText = @"INSERT INTO Karta values(@sifraleta,@brojsedista,@putnik,@klasakarte,@cenakarte,@kapija,1)";


                command.Parameters.Add(new SqlParameter("@sifraleta", k.SifraLeta));
                command.Parameters.Add(new SqlParameter("@brojsedista", k.BrojSedista));
                command.Parameters.Add(new SqlParameter("@putnik", k.KorisnickoIme));
                command.Parameters.Add(new SqlParameter("@klasakarte", k.KlasaKarte));
                command.Parameters.Add(new SqlParameter("@cenakarte", k.CenaKarte));
                command.Parameters.Add(new SqlParameter("@kapija", k.Kapija));
                command.Parameters.Add(new SqlParameter("@aktivan", k.Aktivan));

                command.ExecuteNonQuery();

                connection.Close();

            }

        }

        public void ObrisiKartu(Karta k)
        {
            using (SqlConnection connection = new SqlConnection(CONNECTION_STRING))
            {

                connection.Open();

                SqlCommand command = connection.CreateCommand();
                command.CommandText = @"update Karta set aktivan='0' where sifraleta=@sifraleta";

                command.Parameters.Add(new SqlParameter("@sifraleta", k.SifraLeta));

                command.ExecuteNonQuery();

                connection.Close();

            }
        }

        public void IzmeniKartu(Karta k)
        {
            using (SqlConnection connection = new SqlConnection(CONNECTION_STRING))
            {
                connection.Open();

                SqlCommand command = connection.CreateCommand();
                command.CommandText = @"UPDATE Aerodrom SET brojsedista=@brojsedista, putnik=@putnik, klasakarte=@klasakarte, cenakarte=@cenakarte, kapija=@kapija  WHERE sifraleta=@sifraleta";

                command.Parameters.Add(new SqlParameter("@sifraleta", k.SifraLeta));
                command.Parameters.Add(new SqlParameter("@brojsedista", k.BrojSedista));
                command.Parameters.Add(new SqlParameter("@putnik", k.KorisnickoIme));
                command.Parameters.Add(new SqlParameter("@klasakarte", k.KlasaKarte));
                command.Parameters.Add(new SqlParameter("@cenakarte", k.CenaKarte));
                command.Parameters.Add(new SqlParameter("@kapija", k.Kapija));
                command.Parameters.Add(new SqlParameter("@aktivan", k.Aktivan));

                command.ExecuteNonQuery();

                connection.Close();
            }
        }

    }
}
