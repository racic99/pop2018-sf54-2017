﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using AerodromWPF.Database;
using AerodromWPF.Model;

namespace AerodromWPF
{

    public partial class PutnikProfilPregled : Window
    {
        Korisnik korisnik;

        public PutnikProfilPregled(Korisnik korisnik)
        {
            InitializeComponent();
            this.korisnik = korisnik;

        }

        private void IzmenaButton_Click(object sender, RoutedEventArgs e)
        {

            AddAndEditKorisnikWindow aek = new AddAndEditKorisnikWindow(korisnik, AddAndEditKorisnikWindow.EOpcija.IZMENA);
            aek.CBTipKorisnika.IsEnabled = false;
            aek.ShowDialog();
        }

    }
}
