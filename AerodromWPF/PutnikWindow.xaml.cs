﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using AerodromWPF.Model;

namespace AerodromWPF
{
    public partial class PutnikWindow : Window
    {
        Korisnik korisnik;

        public PutnikWindow(Korisnik korisnik)
        {
            InitializeComponent();
            this.korisnik = korisnik;
            Title = "Glavni prozor - " + korisnik.KorisnickoIme;

        }

        private void ProfilButton_Click(object sender, RoutedEventArgs e)
        {
            PutnikProfilPregled ppp = new PutnikProfilPregled(korisnik);
            ppp.ImeIspisLabel.Content = korisnik.Ime;
            ppp.PrezimeIspisLabel.Content = korisnik.Prezime;
            ppp.KorimeIspisLabel.Content = korisnik.KorisnickoIme;
            ppp.SifraIspisLabel.Content = korisnik.Sifra;
            ppp.EmailIspisLabel.Content = korisnik.Email;
            ppp.AdresaIspisLabel.Content = korisnik.Adresa;
            ppp.PolIspisLabel.Content = korisnik.Pol;
            ppp.Show();

        }

        private void KupljeneKarteButton_Click(object sender, RoutedEventArgs e)
        {
            KupljeneKarteWindow kkw = new KupljeneKarteWindow(korisnik);
            kkw.ShowDialog();
        }

        private void KupiKartuButton_Click(object sender, RoutedEventArgs e)
        {
            KupovinaKarteWindow kkw = new KupovinaKarteWindow(new Karta(), korisnik);
            kkw.ShowDialog();
        }

        private void OdjavaButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
