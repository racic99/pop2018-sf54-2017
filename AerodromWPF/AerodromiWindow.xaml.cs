﻿using AerodromWPF.Database;
using AerodromWPF.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AerodromWPF
{
    public partial class AerodromiWindow : Window
    {
        public AerodromiWindow()
        {
            InitializeComponent();
            DGAerodromi.ItemsSource = Data.Instance.Aerodromi;
            DGAerodromi.IsSynchronizedWithCurrentItem = true;
        }

        private void BtnDodajAerodrom_Click(object sender, RoutedEventArgs e)
        {
            AddAndEditAerodromWindow aew = new AddAndEditAerodromWindow(new Aerodrom());
            aew.ShowDialog();
        }

        private void BtnIzmeniAerodrom_Click(object sender, RoutedEventArgs e)
        {
            Aerodrom selektAerodrom = (Aerodrom) DGAerodromi.SelectedItem;
            if (selektAerodrom != null)
            {
                Aerodrom aAerodrom = (Aerodrom)selektAerodrom.Clone();
                AddAndEditAerodromWindow aew = new AddAndEditAerodromWindow(selektAerodrom, AddAndEditAerodromWindow.EOpcija.IZMENA);
                if (aew.ShowDialog() != true)
                {
                    int index = IndeksAerodroma(selektAerodrom.Sifra);
                    Data.Instance.Aerodromi[index] = aAerodrom;
                }
            }
            else
            {
                MessageBox.Show("Nije selektovan aerodrom! ");
            }
            DGAerodromi.Items.Refresh();
        }

        private void BtnObrisiAerodrom_Click(object sender, RoutedEventArgs e)
        {
            Aerodrom aerodrom = (Aerodrom) DGAerodromi.SelectedItem;
            if (SelektovanAerodrom(aerodrom) && aerodrom.Aktivan)
            {
                if (MessageBox.Show("Da li ste sigurni?", "Potvrda",MessageBoxButton.YesNo).Equals(MessageBoxResult.Yes))
                {
                    int indeks = IndeksAerodroma(aerodrom.Sifra);
                    Data.Instance.Aerodromi[indeks].Aktivan = false;
                    Data.Instance.ObrisiAerodrom(aerodrom);
                }
            }
            
        }

        private int IndeksAerodroma(String sifra)
        {
            int indeks = -1;
            for (int i=0; i<Data.Instance.Aerodromi.Count; i++)
            {
                if (Data.Instance.Aerodromi[i].Sifra.Equals(sifra))
                {
                    indeks = i;
                    break;
                }
            }
            return indeks;
        }

        private bool SelektovanAerodrom(Aerodrom aerodrom)
        {
            if (aerodrom == null)
            {
                MessageBox.Show("Nije selektovan aerodrom! ");
                return false;
            }
            return true;
        }
    }
}
