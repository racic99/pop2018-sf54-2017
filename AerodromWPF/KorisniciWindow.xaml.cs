﻿using AerodromWPF.Database;
using AerodromWPF.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AerodromWPF
{
    public partial class KorisniciWindow : Window
    {

        public KorisniciWindow()
        {
            InitializeComponent();
            DGKorisnici.ItemsSource = Data.Instance.Korisnici;
            DGKorisnici.IsSynchronizedWithCurrentItem = true;
        }

        private void BtnDodajKorisnika_Click(object sender, RoutedEventArgs e)
        {
            AddAndEditKorisnikWindow aek = new AddAndEditKorisnikWindow(new Korisnik());
            aek.ShowDialog();
        }

        private void BtnIzmeniKorisnika_Click(object sender, RoutedEventArgs e)
        {
            Korisnik selektovanKorisnik = (Korisnik)DGKorisnici.SelectedItem;
            if (selektovanKorisnik != null)
            {
                Korisnik kKorisnik = (Korisnik)selektovanKorisnik.Clone();
                AddAndEditKorisnikWindow aek = new AddAndEditKorisnikWindow(selektovanKorisnik, AddAndEditKorisnikWindow.EOpcija.IZMENA);
                if (aek.ShowDialog() != true)
                {
                    int index = IndeksKorisnika(selektovanKorisnik.KorisnickoIme);
                    Data.Instance.Korisnici[index] = kKorisnik;
                }
            }
        }

        private void BtnObrisiKorisnika_Click(object sender, RoutedEventArgs e)
        {
            Korisnik korisnik = (Korisnik)DGKorisnici.SelectedItem;
            if (SelektovanKorisnik(korisnik) && korisnik.Aktivan)
            {
                if (MessageBox.Show("Da li ste sigurni?", "Potvrda", MessageBoxButton.YesNo).Equals(MessageBoxResult.Yes))
                {
                    int indeks = IndeksKorisnika(korisnik.KorisnickoIme);
                    Data.Instance.Korisnici[indeks].Aktivan = false;
                    Data.Instance.ObrisiKorisnika(korisnik);
                }
            }
        }

        private int IndeksKorisnika(String korisnickoime)
        {
            int indeks = -1;
            for (int i = 0; i < Data.Instance.Korisnici.Count; i++)
            {
                if (Data.Instance.Korisnici[i].KorisnickoIme.Equals(korisnickoime))
                {
                    indeks = i;
                    break;
                }
            }
            return indeks;
        }

        private bool SelektovanKorisnik(Korisnik korisnik)
        {
            if (korisnik == null)
            {
                MessageBox.Show("Nije selektovan korisnik! ");
                return false;
            }
            return true;
        }
    }
}
