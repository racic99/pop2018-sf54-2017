﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using AerodromWPF.Database;
using AerodromWPF.Model;

namespace AerodromWPF
{
    public partial class KupljeneKarteWindow : Window
    {
        Korisnik korisnik;
        private ObservableCollection<Karta> Karte { get; set; }
        public KupljeneKarteWindow(Korisnik korisnik)

        {
            InitializeComponent();
            Karte = new ObservableCollection<Karta>();
            this.korisnik = korisnik;
            Title = "Kupljene karte - " + korisnik.KorisnickoIme;
            foreach(Karta k in Data.Instance.Karte)
            {
                if (k.KorisnickoIme.Equals(korisnik.KorisnickoIme) && k.Aktivan)
                {
                    Karte.Add(k);
                    DGKarte.ItemsSource = Karte;
                    DGKarte.IsSynchronizedWithCurrentItem = true;
                }
            }

        }

        private void IzlazButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
