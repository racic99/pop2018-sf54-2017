﻿using AerodromWPF.Database;
using AerodromWPF.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AerodromWPF
{
    public partial class AvioniWindow : Window
    {
        public AvioniWindow()
        {
            InitializeComponent();
            DGAvioni.ItemsSource = Data.Instance.Avioni;
            DGAvioni.IsSynchronizedWithCurrentItem = true;
        }

        private void BtnDodajAvion_Click(object sender, RoutedEventArgs e)
        {
            AddAndEditAvionWindow aea = new AddAndEditAvionWindow(new Avion());
            aea.ShowDialog();
        }

        private void BtnIzmeniAvion_Click(object sender, RoutedEventArgs e)
        {
            Avion selektovanAvion = (Avion)DGAvioni.SelectedItem;
            if (selektovanAvion != null)
            {
                Avion aAvion = (Avion)selektovanAvion.Clone();
                AddAndEditAvionWindow aea = new AddAndEditAvionWindow(selektovanAvion, AddAndEditAvionWindow.EOpcija.IZMENA);
                if (aea.ShowDialog() == true)
                {
                    int index = IndeksAviona(selektovanAvion.BrojLeta);
                    Data.Instance.Avioni[index] = aAvion;
                }
            }
            else
            {
                MessageBox.Show("Nije selektovan avion");
            }
            DGAvioni.Items.Refresh();
        }

        private void BtnObrisiAvion_Click(object sender, RoutedEventArgs e)
        {
            Avion avion = (Avion)DGAvioni.SelectedItem;
            if (SelektovanAvion(avion) && avion.Aktivan)
            {
                if (MessageBox.Show("Da li ste sigurni?", "Potvrda", MessageBoxButton.YesNo).Equals(MessageBoxResult.Yes))
                {
                    int indeks = IndeksAviona(avion.BrojLeta);
                    Data.Instance.Avioni[indeks].Aktivan = false;
                    Data.Instance.ObrisiAvion(avion);
                }
            }
        }

        private int IndeksAviona(String brojLeta)
        {
            int indeks = -1;
            for (int i = 0; i < Data.Instance.Avioni.Count; i++)
            {
                if (Data.Instance.Avioni[i].BrojLeta.Equals(brojLeta))
                {
                    indeks = i;
                    break;
                }
            }
            return indeks;
        }

        private bool SelektovanAvion(Avion avion)
        {
            if (avion == null)
            {
                MessageBox.Show("Nije selektovan avion! ");
                return false;
            }
            return true;
        }
    }
}

