﻿using AerodromWPF.Database;
using AerodromWPF.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AerodromWPF
{
    public partial class AddAndEditLetWindow : Window
    {
        public enum EOpcija { DODAVANJE, IZMENA }
        private EOpcija opcija;
        private Let let;
        public AddAndEditLetWindow(Let let, EOpcija opcija = EOpcija.DODAVANJE)
        {
            InitializeComponent();
            this.let = let;
            this.opcija = opcija;


            this.DataContext = let;

            CBDestinacija.ItemsSource = Data.Instance.Aerodromi.Select(a => a.Sifra);
            CBOdrediste.ItemsSource = Data.Instance.Aerodromi.Select(a => a.Sifra);

            if (opcija.Equals(EOpcija.IZMENA))
            {
                TxtBrojLeta.IsEnabled = false;

            }
        }


        private bool LetPostoji(string brojleta)
        {
            foreach (Let l in Data.Instance.Letovi)
            {
                if (l.BrojLeta.Equals(brojleta))
                {
                    return true;
                }
            }
            return false;
        }

        private void BtnSacuvaj_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(TxtPilot.Text) || string.IsNullOrEmpty(TxtBrojLeta.Text) || string.IsNullOrEmpty(CBDestinacija.Text) ||
                string.IsNullOrEmpty(CBOdrediste.Text) || string.IsNullOrEmpty(TxtCenaKarte.Text) || string.IsNullOrEmpty(DTVremePolaska.Text) ||
                string.IsNullOrEmpty(DTVremeDolaska.Text))
            {
                MessageBox.Show("Sva polja moraju biti popunjena");
            }

            else if (opcija.Equals(EOpcija.IZMENA))
            {
                this.DialogResult = true;
                Data.Instance.IzmeniLet(let);

                this.Close();
            }
            else if (opcija.Equals(EOpcija.DODAVANJE) && !LetPostoji(let.BrojLeta))
            {
                this.DialogResult = true;
                Data.Instance.Letovi.Add(let);
                Data.Instance.DodajLet(let);

                this.Close();
            }
            else
            {
                MessageBox.Show("Vec postoji let sa tom sifrom");
            }

        }

        private void BtnOdustani_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

    }
}


