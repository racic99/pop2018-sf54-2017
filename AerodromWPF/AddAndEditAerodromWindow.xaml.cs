﻿using AerodromWPF.Database;
using AerodromWPF.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AerodromWPF
{
    public partial class AddAndEditAerodromWindow : Window
    {
        public enum EOpcija { DODAVANJE, IZMENA }
        private EOpcija opcija;
        private Aerodrom aerodrom;
        public AddAndEditAerodromWindow(Aerodrom aerodrom, EOpcija opcija = EOpcija.DODAVANJE)
        {
            InitializeComponent();
            this.aerodrom = aerodrom;
            this.opcija = opcija;

            this.DataContext = aerodrom;
            TxtSifra.CharacterCasing = CharacterCasing.Upper;

            if (opcija.Equals(EOpcija.IZMENA))
            {
                TxtSifra.IsEnabled = false;

            }
        }

        private bool AerodromPostoji(string sifra)
        {
            foreach (Aerodrom a in Data.Instance.Aerodromi)
            {
                if(a.Sifra.Equals(sifra))
                {
                    return true;
                }
            }
            return false;
        }

        private bool SifraDuzina()
        {
            if (TxtSifra.Text.Length > 3)
            {
                return true;
            }
            return false;
        }

        private void Sacuvaj_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(TxtSifra.Text) || string.IsNullOrEmpty(TxtIme.Text) || string.IsNullOrEmpty(TxtGrad.Text))
            {
                MessageBox.Show("Sva polja moraju biti popunjena");
            }
            else if (opcija.Equals(EOpcija.IZMENA))
            {
                this.DialogResult = true;
                Data.Instance.IzmeniAerodrom(aerodrom);

                this.Close();
            }
            else if (opcija.Equals(EOpcija.DODAVANJE))
            {
                if (!AerodromPostoji(aerodrom.Sifra) && !SifraDuzina())
                {


                    this.DialogResult = true;
                    Data.Instance.DodajAerodrom(aerodrom);
                    Data.Instance.Aerodromi.Add(aerodrom);

                    this.Close();
                }
                else
                {
                    MessageBox.Show("Vec postoji aerodrom sa tom sifrom ili ste uneli sifru duzu od 3 karaktera");
                }
            }

        }

        private void Odustani_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}
