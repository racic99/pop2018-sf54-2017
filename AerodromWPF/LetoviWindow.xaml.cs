﻿using AerodromWPF.Database;
using AerodromWPF.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AerodromWPF
{
    public partial class LetoviWindow : Window
    {
        public LetoviWindow()
        {
            InitializeComponent();
            DGLetovi.ItemsSource = Data.Instance.Letovi;
            DGLetovi.IsSynchronizedWithCurrentItem = true;

        }

        private void BtnDodajLet_Click(object sender, RoutedEventArgs e)
        {
            AddAndEditLetWindow ael = new AddAndEditLetWindow(new Let());
            ael.ShowDialog();
        }

        private void BtnIzmeniLet_Click(object sender, RoutedEventArgs e)
        {
            Let selektovanLet = (Let)DGLetovi.SelectedItem;
            if (selektovanLet != null) 
            {
                Let lLet = (Let)selektovanLet.Clone();
                AddAndEditLetWindow ael = new AddAndEditLetWindow(selektovanLet, AddAndEditLetWindow.EOpcija.IZMENA);
                if (ael.ShowDialog() == true)
                {
                    int index = IndeksLeta(selektovanLet.BrojLeta);
                    Data.Instance.Letovi[index] = lLet;
                }
            }
        }

        private void BtnObrisiLet_Click(object sender, RoutedEventArgs e)
        {
            Let let = (Let)DGLetovi.SelectedItem;
            if (SelektovanLet(let) && let.Aktivan) 
            {
                if (MessageBox.Show("Da li ste sigurni?", "Potvrda", MessageBoxButton.YesNo).Equals(MessageBoxResult.Yes))
                {
                    int indeks = IndeksLeta(let.BrojLeta);
                    Data.Instance.Letovi[indeks].Aktivan = false;
                    Data.Instance.ObrisiLet(let);
                }
            }
        }

        private int IndeksLeta(String sifra)
        {
            int indeks = -1;
            for (int i = 0; i < Data.Instance.Letovi.Count; i++)
            {
                if (Data.Instance.Letovi[i].BrojLeta.Equals(sifra))
                {
                    indeks = i;
                    break;
                }
            }
            return indeks;
        }

        private bool SelektovanLet(Let let)
        {
            if (let == null)
            {
                MessageBox.Show("Nije selektovan let! ");
                return false;
            }
            return true;
        }
    }
}

