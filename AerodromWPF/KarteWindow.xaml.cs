﻿using AerodromWPF.Database;
using AerodromWPF.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AerodromWPF
{
    public partial class KarteWindow : Window
    {
        public KarteWindow()
        {
            InitializeComponent();
            DGKarte.ItemsSource = Data.Instance.Karte;
            DGKarte.IsSynchronizedWithCurrentItem = true;
        }

        private void BtnDodajKartu_Click(object sender, RoutedEventArgs e)
        {
            KupovinaKarteAdminWindow kkaw = new KupovinaKarteAdminWindow(new Karta());
            kkaw.ShowDialog();
        }

        private void BtnIzmeniKartu_Click(object sender, RoutedEventArgs e)
        {
            Karta selektKarta = (Karta)DGKarte.SelectedItem;
            if (selektKarta != null)
            {
                Karta kKarta = (Karta)selektKarta.Clone();
                KupovinaKarteAdminWindow kkaw = new KupovinaKarteAdminWindow(selektKarta, KupovinaKarteAdminWindow.EOpcija.IZMENA);
                if (kkaw.ShowDialog() != true)
                {
                    int index = IndeksKarte(selektKarta.SifraLeta);
                    Data.Instance.Karte[index] = kKarta;
                }
            }
            else
            {
                MessageBox.Show("Nije selektovana karta! ");
            }
            DGKarte.Items.Refresh();
        }

        private void BtnObrisiKartu_Click(object sender, RoutedEventArgs e)
        {
            Karta karta = (Karta)DGKarte.SelectedItem;
            if (SelektovanaKarta(karta) && karta.Aktivan)
            {
                if (MessageBox.Show("Da li ste sigurni?", "Potvrda", MessageBoxButton.YesNo).Equals(MessageBoxResult.Yes))
                {
                    int indeks = IndeksKarte(karta.SifraLeta);
                    Data.Instance.Karte[indeks].Aktivan = false;
                    Data.Instance.ObrisiKartu(karta);
                }
            }

        }

        private int IndeksKarte(String sifraleta)
        {
            int indeks = -1;
            for (int i = 0; i < Data.Instance.Karte.Count; i++)
            {
                if (Data.Instance.Karte[i].SifraLeta.Equals(sifraleta))
                {
                    indeks = i;
                    break;
                }
            }
            return indeks;
        }

        private bool SelektovanaKarta(Karta karta)
        {
            if (karta == null)
            {
                MessageBox.Show("Nije selektovana karta! ");
                return false;
            }
            return true;
        }
    }
} 
