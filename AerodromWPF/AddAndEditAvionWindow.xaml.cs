﻿using AerodromWPF.Database;
using AerodromWPF.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AerodromWPF
{
    public partial class AddAndEditAvionWindow : Window
    {
        public enum EOpcija { DODAVANJE, IZMENA }
        private EOpcija opcija;
        private Avion avion;
        public AddAndEditAvionWindow(Avion avion, EOpcija opcija = EOpcija.DODAVANJE)
        {
            InitializeComponent();
            this.avion = avion;
            this.opcija = opcija;

            this.DataContext = avion;

            CBNazivAviokompanije.ItemsSource = Data.Instance.Aviokompanije.Select(a => a.Naziv);
            CBBrojLeta.ItemsSource = Data.Instance.Letovi.Select(l => l.BrojLeta);
            TxtBizKlasa.Text = "10";
            TxtEkoKlasa.Text = "20";
            TxtBizKlasa.IsEnabled = false;
            TxtEkoKlasa.IsEnabled = false;

            if (opcija.Equals(EOpcija.IZMENA))
            {
                CBBrojLeta.IsEnabled = false;

            }
        }

        private bool AvionPostoji(string brojLeta)
        {
            foreach (var a in Data.Instance.Avioni)
            {
                if (a.BrojLeta.Equals(brojLeta))
                {
                    return true;
                }
            }
            return false;
        }

        private void BtnSacuvaj_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(CBBrojLeta.Text) || string.IsNullOrEmpty(CBNazivAviokompanije.Text))
            {
                MessageBox.Show("Sva polja moraju biti popunjena");
            }

            else if (opcija.Equals(EOpcija.IZMENA))
            {
                avion.BrojLeta = CBBrojLeta.Text;
                avion.SedistaBiznis = TxtBizKlasa.Text;
                avion.SedistaEkonomska = TxtEkoKlasa.Text;
                avion.NazivKompanije = CBNazivAviokompanije.Text;
                this.DialogResult = true;
                Data.Instance.IzmeniAvion(avion);
                this.Close();
            }

            else if (opcija.Equals(EOpcija.DODAVANJE) && !AvionPostoji(avion.BrojLeta))
            {

                    avion.BrojLeta = CBBrojLeta.Text;
                    avion.SedistaBiznis = TxtBizKlasa.Text;
                    avion.SedistaEkonomska = TxtEkoKlasa.Text;
                    avion.NazivKompanije = CBNazivAviokompanije.Text;
                    this.DialogResult = true;
                    Data.Instance.Avioni.Add(avion);
                    Data.Instance.DodajAvion(avion);
                    this.Close();
            }
            else
            {
                    MessageBox.Show("Vec postoji avion sa tim brojem leta");
            }

        }

        private void BtnOdustani_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}

